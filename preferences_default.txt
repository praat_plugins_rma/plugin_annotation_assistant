run_plugin.textgrid_dir: /home/rolando/Desktop/corpus
run_plugin.audio_dir: /home/rolando/Desktop/corpus
run_plugin.all_tier_names: tone phoneme word
run_plugin.point_tiers: tone
run_plugin.deep_search: 0
run_plugin.show: 1
run_plugin.recursive_search: 0
create_textgrid.all_tier_names: tone phoneme word
create_textgrid.point_tiers: tone
create_textgrid.recursive_search: 0
create_textgrid.recursive_search.textgrid_dir: 
insert_tier.all_tier_names: tone phoneme word
insert_tier.point_tiers: tone
insert_tier.recursive_search: 0
replace_tier_text.tier_name: phon
replace_tier_text.search: a
replace_tier_text.replace: a
replace_tier_text.mode: 1
replace_tier_text.recursive_search: 0
duplicate_tier.tier_name: text.orth
duplicate_tier.after_tier: 
duplicate_tier.new_tier_name: text.phon
duplicate_tier.recursive_search: 0
remove_tier.all_tier_names: phon word
remove_tier.delete_textgrid: 1
remove_tier.recursive_search: 0
set_tier_name.tier_name: Phoneme Phon ph
set_tier_name.new_name: phon
set_tier_name.recursive_search: 0
audio_extension: .wav
adjust_volume: 0
open_as_LongSound: 0
spectrogram.min_range: 0
spectrogram.max_range: 5000
spectrogram.dynamic_range: 60
spectrogram.view_lenght: 0.005
analysis.pitch: 0
analysis.intensity: 0
analysis.formants: 0
analysis.pulse: 0
textGridEditor.default_values: 0
audio_dir: /home/rolando/Desktop/corpus
textgrid_dir: /home/rolando/Desktop/corpus
get_textgrid_report.recursive_search: 0
get_textgrid_report.rich_report: 0
audio_extension: .wav
