# Annotation assistant

This plug-in provides you with some tools that are helpful when annotating a set of audio files.

## Features

- Insert tier
- Search level:
  - TextGrid
  - Tier
- Show:
  - New cases
  - Complete cases
  - All cases
- Query


## Getting Started

## Author

- Rolando Muñoz

## License

This project is licensed under the GPL3 - see the [LICENSE.md](LICENSE.md) file for details
